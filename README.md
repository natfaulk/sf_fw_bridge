# FW Bridge
This script is to interact with badens new boards. The boards have to be polled for data unlike the old ones which automatically send the data. Sends the data to the app.  

## FW Bridge Help
----------------

Set id:  
--id=123 where 123 is the id. Defaults to 0

Set data interval  
--interval=100 data send interval in milliseconds. Defaults to 100ms

Set host address:  
--host=127.0.0.1 Defaults to 127.0.0.1  
Can use domain name ie test.com or ip  
Note this input is not validated. If there is a server error check this parameter

Set port:  
--port=3000 Defaults to 3000

Display help:  
help or --help or -h