const SerialPort = require('serialport')
const http = require('http')

const IDS = ['0','1','2','3']

let lastinput = 0
const TIMEOUT = 1000

;(() => {
  let settings = parseArguements()
  if (settings === null) return

  const port = new SerialPort('COM6', {
    baudRate: 115200
  })

  port.on('error', err => {
    console.log('Error: ', err.message)
  })

  let buff = ''

  port.on('data', data => {
    buff += data.toString('utf8')
    let msgs = buff.split('\n')

    if (msgs.length > 1) {
      let latest = msgs.shift()

      console.log(latest)
      let d = latest.trim().split(',')
      
      if (d[d.length - 1] == '') d.pop()
      let out = {ID: `NEW${IDS[settings.id]}`}
    
      d.forEach((_d, _i) => {
        out[`p${_i + 1}`] = _d
      })
  
      sendData(out, settings)

      getNext(settings, port)
    }
    if (msgs.length > 0) buff = msgs[0]
  })

  getNext(settings, port)

  setInterval(() => {
    if ((Date.now() - lastinput) > TIMEOUT) {
      getNext(settings, port)
    }
  }, 1000)
})()

function getNext(_settings, _port) {
  lastinput = Date.now()
  _settings.id++
  _settings.id %= IDS.length
  _port.write(IDS[_settings.id])
}

let sendData = (_data, _settings) => {
  let postData = JSON.stringify(_data)
  let options = {
    host: _settings.dest,
    path: '/submit',
    port: _settings.port,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
  }

  let req = http.request(options, res => {
    let data = ''

    res.on('data', chunk => {
      data += chunk
    })

    res.on('end', () => {
      console.log(data);
    })
  }).on('error', err => {
    console.log(`Error: ${err.message}`)
  })

  req.write(postData)
}

function displayHelp() {
  console.log('\nFW Bridge Help')
  console.log('----------------\r\n')
  console.log('Set data interval')
  console.log('--interval=100 data send interval in milliseconds. Defaults to 100ms\n')
  console.log('Set host address:')
  console.log('--host=127.0.0.1 Defaults to 127.0.0.1')
  console.log('Can use domain name ie test.com or ip')
  console.log('Note this input is not validated. If there is a server error check this parameter\n')
  console.log('Set port:')
  console.log('--port=3000 Defaults to 3000\n')
  console.log('Display help:')
  console.log('help or --help or -h\n')
}

function parseArguements() {
  let trimArg = _arg => {
    return _arg.substring(_arg.indexOf('=') + 1)
  }

  let settings = {
    id: 0,
    dest: '127.0.0.1',
    port: 3000,
    interval: 100
  }

  let exitProgram = false

  process.argv.slice(2).forEach(arg => {
    if (arg.startsWith('--host=')) {
      settings.dest = trimArg(arg)
      console.log(`Host address set to ${settings.dest}`)
    } else if (arg.startsWith('--port=')) {
      let tempPort = parseInt(trimArg(arg))
      if (isNaN(tempPort)) console.log(`Port not a number. Ignoring...`)
      else {
        settings.port = tempPort
        console.log(`Port set to ${settings.port}`)
      }
    } else if (arg.startsWith('--interval=')) {
      let tempInterval = parseInt(trimArg(arg))
      if (isNaN(tempInterval)) console.log(`Interval not a number. Ignoring...`)
      else {
        settings.interval = tempInterval
        console.log(`Interval set to ${settings.interval}`)
      }
    } else if (arg == 'help' || arg == '--help' || arg == '-h') {
      displayHelp()
      exitProgram = true
    } else console.log(`Unknown argument: ${arg}`)
  })

  if (exitProgram) return null
  else return settings
}
